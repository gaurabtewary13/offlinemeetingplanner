import 'package:flutter/material.dart';
import 'package:offlinemeetingplanner/Misc/Constants.dart';

import 'Misc/LocalNotificationHandler.dart';
import 'Misc/MeetingRouter.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await LoginNotificationHandler().setUpLocalNotifications();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.purple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      onGenerateRoute: MeetingRouter.generateRoute,
      initialRoute: homeViewRoute,
    );
  }
}
