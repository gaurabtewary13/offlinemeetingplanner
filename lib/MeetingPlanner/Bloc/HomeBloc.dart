import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Model/Meeting.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Model/Settings.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Respository/MeetingRepository.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Respository/SettingsRepository.dart';

class HomeBloc extends Bloc<HomeViewEvent, HomeViewState> {
  HomeBloc({
    @required this.repository,
    @required this.settingsRepository,
  }) : super(
          HomeViewState(
            selectedDate: DateTime.now(),
          ),
        );
  final MeetingRepository repository;
  final SettingsRepository settingsRepository;

  @override
  Stream<HomeViewState> mapEventToState(HomeViewEvent event) async* {
    if (event is FetchMeetings) {
      final HomeViewState newState = HomeViewState(
        selectedDate: event.date,
        settings: state.settings,
        meetings: state.meetings,
      );
      yield newState;
      yield await _fetchMeetings(event.date);
    } else if (event is FetchData) {
      yield await _fetchData(event.date);
    }
  }

  Future<HomeViewState> _fetchData(
    DateTime date,
  ) async {
    Settings settings = await settingsRepository.fetchSettings();
    List<Meeting> meetings = await repository.fetchMeetings(date);
    final HomeViewState newState = HomeViewState(
      settings: settings,
      selectedDate: date,
      meetings: meetings,
    );
    return newState;
  }

  Future<HomeViewState> _fetchMeetings(
    DateTime date,
  ) async {
    HomeViewState newState = HomeViewState(
      selectedDate: state.selectedDate,
      settings: state.settings,
    );
    List<Meeting> meetings = await repository.fetchMeetings(date);
    newState.meetings = meetings;

    return newState;
  }
}

class HomeViewState {
  HomeViewState({
    this.selectedDate,
    this.meetings,
    this.settings,
  });
  final DateTime selectedDate;
  List<Meeting> meetings;
  Settings settings;
}

abstract class HomeViewEvent {}

class FetchMeetings extends HomeViewEvent {
  FetchMeetings({
    @required this.date,
  });
  DateTime date;
}

class FetchData extends HomeViewEvent {
  FetchData({
    @required this.date,
  });
  DateTime date;
}
