import 'dart:math';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Model/Meeting.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Model/Settings.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Respository/MeetingRepository.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Respository/SettingsRepository.dart';

class NewBookingBloc extends Bloc<NewBookingBlocEvent, NewBookingState> {
  NewBookingBloc({
    @required this.repository,
    @required this.settingsRepository,
  }) : super(
          NewBookingState(
            meeting: Meeting(
              id: Random().nextInt(1000),
              date: DateTime.now(),
              from: TimeOfDay.fromDateTime(
                DateTime.now(),
              ),
              to: TimeOfDay.fromDateTime(
                DateTime.now().add(
                  Duration(minutes: 30),
                ),
              ),
              room: 0,
              priority: 0,
              reminderTime: 0,
            ),
          ),
        );
  final MeetingRepository repository;
  final SettingsRepository settingsRepository;

  @override
  Stream<NewBookingState> mapEventToState(NewBookingBlocEvent event) async* {
    if (event is UpdateTitle) {
      yield _updateTitle(event.title);
    } else if (event is UpdateDescription) {
      yield _updateDescription(event.description);
    } else if (event is UpdateStartDate) {
      yield _updateDate(event.date);
    } else if (event is UpdateStartTime) {
      yield _updateStartTime(event.time);
    } else if (event is UpdateEndTime) {
      yield _updateEndTime(event.time);
    } else if (event is UpdateRoom) {
      yield _updateRoom(event.room);
    } else if (event is UpdatePriority) {
      yield _updatePriority(event.priority);
    } else if (event is UpdateReminderTime) {
      yield _updateReminder(event.reminderTime);
    } else if (event is SaveMeeting) {
      yield await _saveMeeting();
    } else if (event is FetchSettings) {
      yield await _fetchSettings();
    }
  }

  Future<NewBookingState> _fetchSettings() async {
    Settings settings = await settingsRepository.fetchSettings();
    final NewBookingState bookingState =
        NewBookingState(meeting: state.meeting);
    bookingState.settings = settings;
    return bookingState;
  }

  NewBookingState _updateTitle(String title) {
    Meeting meeting = state.meeting;
    meeting.title = title;
    final NewBookingState bookingState = NewBookingState(meeting: meeting, settings: state.settings);
    return bookingState;
  }

  NewBookingState _updateDescription(String desc) {
    Meeting meeting = state.meeting;
    meeting.desc = desc;
    final NewBookingState bookingState = NewBookingState(meeting: meeting, settings: state.settings);
    return bookingState;
  }

  NewBookingState _updateDate(DateTime date) {
    Meeting meeting = state.meeting;
    meeting.date = date;
    final NewBookingState bookingState = NewBookingState(meeting: meeting, settings: state.settings);
    return bookingState;
  }

  NewBookingState _updateStartTime(TimeOfDay time) {
    Meeting meeting = state.meeting;
    meeting.from = time;
    final NewBookingState bookingState = NewBookingState(meeting: meeting, settings: state.settings);
    return bookingState;
  }

  NewBookingState _updateEndTime(TimeOfDay time) {
    Meeting meeting = state.meeting;
    meeting.to = time;
    final NewBookingState bookingState = NewBookingState(meeting: meeting, settings: state.settings);
    return bookingState;
  }

  NewBookingState _updateRoom(int room) {
    Meeting meeting = state.meeting;
    meeting.room = room;
    final NewBookingState bookingState = NewBookingState(meeting: meeting, settings: state.settings);
    return bookingState;
  }

  NewBookingState _updatePriority(int priority) {
    Meeting meeting = state.meeting;
    meeting.priority = priority;
    final NewBookingState bookingState = NewBookingState(meeting: meeting, settings: state.settings);
    return bookingState;
  }

  NewBookingState _updateReminder(int remiderTime) {
    Meeting meeting = state.meeting;
    meeting.reminderTime = remiderTime;
    final NewBookingState bookingState = NewBookingState(meeting: meeting, settings: state.settings);
    return bookingState;
  }

  Future<NewBookingState> _saveMeeting() async {
    return repository
        .insertMeeting(
      state.meeting,
    )
        .then((onValue) {
      return NewBookingState(meeting: state.meeting, isSaved: true, settings: state.settings);
    }).catchError((onError) {
      debugPrint('Meeting save error - $onError');
      return NewBookingState(meeting: state.meeting, isSaved: false, settings: state.settings);
    });
  }
}

class NewBookingState {
  NewBookingState({
    this.meeting,
    this.settings,
    this.isSaved = false,
  });
  Meeting meeting;
  Settings settings;
  bool isSaved;
}

abstract class NewBookingBlocEvent {}

class FetchSettings extends NewBookingBlocEvent {}

class UpdateTitle extends NewBookingBlocEvent {
  UpdateTitle({
    @required this.title,
  });
  String title;
}

class UpdateDescription extends NewBookingBlocEvent {
  UpdateDescription({
    @required this.description,
  });
  String description;
}

class UpdateStartDate extends NewBookingBlocEvent {
  UpdateStartDate({
    @required this.date,
  });
  DateTime date;
}

class UpdateStartTime extends NewBookingBlocEvent {
  UpdateStartTime({
    @required this.time,
  });
  TimeOfDay time;
}

class UpdateEndTime extends NewBookingBlocEvent {
  UpdateEndTime({
    @required this.time,
  });
  TimeOfDay time;
}

class UpdateRoom extends NewBookingBlocEvent {
  UpdateRoom({
    @required this.room,
  });
  int room;
}

class UpdatePriority extends NewBookingBlocEvent {
  UpdatePriority({
    @required this.priority,
  });
  int priority;
}

class UpdateReminderTime extends NewBookingBlocEvent {
  UpdateReminderTime({
    @required this.reminderTime,
  });
  int reminderTime;
}

class SaveMeeting extends NewBookingBlocEvent {
  SaveMeeting({
    @required this.meeting,
  });
  Meeting meeting;
}
