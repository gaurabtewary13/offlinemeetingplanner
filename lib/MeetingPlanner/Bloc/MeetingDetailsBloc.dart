import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Model/Meeting.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Respository/MeetingRepository.dart';

class MeetingDetailsBloc
    extends Bloc<MeetingDetailsEvent, MeetingDetailsState> {
  MeetingDetailsBloc({
    @required this.repository,
  }) : super(
          MeetingDetailsState(),
        );
  final MeetingRepository repository;

  @override
  Stream<MeetingDetailsState> mapEventToState(
      MeetingDetailsEvent event) async* {
    if (event is DeleteMeeting) {
      yield await _deleteMeeting(event.meeting);
    }
  }

  Future<MeetingDetailsState> _deleteMeeting(Meeting meeting) async {
    return repository.deleteMeeting(meeting).then((onValue) {
      return MeetingDetailsState(isDeleted: true);
    }).catchError((onError) {
      debugPrint('Error in deleting meeting - $onError');
      MeetingDetailsState(isDeleted: false);
    });
  }
}

class MeetingDetailsState {
  MeetingDetailsState({
    this.isDeleted = false,
  });
  bool isDeleted;
}

abstract class MeetingDetailsEvent {}

class DeleteMeeting extends MeetingDetailsEvent {
  DeleteMeeting({
    @required this.meeting,
  });
  Meeting meeting;
}
