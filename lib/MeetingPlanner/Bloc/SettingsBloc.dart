import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Model/Settings.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Respository/SettingsRepository.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  SettingsBloc({
    @required this.repository,
  }) : super(
          SettingsState(),
        );
  final SettingsRepository repository;

  @override
  Stream<SettingsState> mapEventToState(SettingsEvent event) async* {
    if (event is FetchSettings) {
      yield await _fetchSettings();
    } else if (event is UpdateOfficeStartTime) {
      yield await _updateOfficeStartTime(event.time);
    } else if (event is UpdateOfficeEndTime) {
      yield await _updateOfficeEndTime(event.time);
    } else if (event is UpdateRoom1Color) {
      yield await _updateRoom1Color(event.colorCode);
    }else if (event is UpdateRoom2Color) {
      yield await _updateRoom2Color(event.colorCode);
    }else if (event is UpdateRoom3Color) {
      yield await _updateRoom3Color(event.colorCode);
    }else if (event is UpdateRoom4Color) {
      yield await _updateRoom4Color(event.colorCode);
    }else if (event is UpdateReminder) {
      yield await _updateReminderTime(event.flag);
    }
  }

  Future<SettingsState> _fetchSettings() async {
    Settings settings = await repository.fetchSettings();
    final SettingsState settingsState = SettingsState(settings: settings);
    return settingsState;
  }

  Future<SettingsState> _updateOfficeStartTime(TimeOfDay time) async {
    Settings settings = state.settings;
    settings.officeStartTime = time;
    await repository.updateSettings(settings);
    final SettingsState settingsState = SettingsState(settings: settings);
    return settingsState;
  }

  Future<SettingsState> _updateOfficeEndTime(TimeOfDay time) async {
    Settings settings = state.settings;
    settings.officeEndTime = time;
    await repository.updateSettings(settings);
    final SettingsState settingsState = SettingsState(settings: settings);
    return settingsState;
  }

  Future<SettingsState> _updateRoom1Color(String colorCode) async {
    Settings settings = state.settings;
    settings.room1Color = colorCode;
    await repository.updateSettings(settings);
    final SettingsState settingsState = SettingsState(settings: settings);
    return settingsState;
  }

  Future<SettingsState> _updateRoom2Color(String colorCode) async {
    Settings settings = state.settings;
    settings.room2Color = colorCode;
    await repository.updateSettings(settings);
    final SettingsState settingsState = SettingsState(settings: settings);
    return settingsState;
  }

  Future<SettingsState> _updateRoom3Color(String colorCode) async {
    Settings settings = state.settings;
    settings.room3Color = colorCode;
    await repository.updateSettings(settings);
    final SettingsState settingsState = SettingsState(settings: settings);
    return settingsState;
  }

  Future<SettingsState> _updateRoom4Color(String colorCode) async {
    Settings settings = state.settings;
    settings.room4Color = colorCode;
    await repository.updateSettings(settings);
    final SettingsState settingsState = SettingsState(settings: settings);
    return settingsState;
  }

  Future<SettingsState> _updateReminderTime(bool flag) async {
    Settings settings = state.settings;
    settings.isReminderOn = flag;
    await repository.updateSettings(settings);
    final SettingsState settingsState = SettingsState(settings: settings);
    return settingsState;
  }
}

class SettingsState {
  SettingsState({
    this.settings,
  });
  final Settings settings;
}

abstract class SettingsEvent {}

class FetchSettings extends SettingsEvent {}

class UpdateOfficeStartTime extends SettingsEvent {
  UpdateOfficeStartTime({this.time});
  final TimeOfDay time;
}

class UpdateOfficeEndTime extends SettingsEvent {
  UpdateOfficeEndTime({this.time});
  final TimeOfDay time;
}

class UpdateRoom1Color extends SettingsEvent {
  UpdateRoom1Color({this.colorCode});
  final String colorCode;
}

class UpdateRoom2Color extends SettingsEvent {
  UpdateRoom2Color({this.colorCode});
  final String colorCode;
}

class UpdateRoom3Color extends SettingsEvent {
  UpdateRoom3Color({this.colorCode});
  final String colorCode;
}

class UpdateRoom4Color extends SettingsEvent {
  UpdateRoom4Color({this.colorCode});
  final String colorCode;
}

class UpdateReminder extends SettingsEvent {
  UpdateReminder({this.flag});
  final bool flag;
}
