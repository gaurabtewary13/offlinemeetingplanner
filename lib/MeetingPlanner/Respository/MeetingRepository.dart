import 'package:flutter/material.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Model/Meeting.dart';
import 'package:offlinemeetingplanner/Misc/DBConfiguration.dart';
import 'package:sqflite/sqflite.dart';

class MeetingRepository {
  Future<void> insertMeeting(Meeting meeting) async {
    final Database db = await DBConfiguration.openDB();
    await db.insert(
      'Meeting',
      meeting.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Meeting>> fetchMeetings(DateTime date) async {
    final Database db = await DBConfiguration.openDB();
    final List<Map<String, dynamic>> maps = await db.query(
      'Meeting',
    );
    DateTime dateWithOutTime = DateTime(date.year, date.month, date.day);

    List list = List.generate(maps.length, (i) {
      DateTime from =
          DateTime.fromMillisecondsSinceEpoch((maps[i]['fromTime']).toInt());
      DateTime to =
          DateTime.fromMillisecondsSinceEpoch((maps[i]['toTime']).toInt());
      return Meeting(
        id: maps[i]['id'],
        title: maps[i]['title'],
        desc: maps[i]['desc'],
        date: DateTime(from.year, from.month, from.day),
        from: TimeOfDay(hour: from.hour, minute: from.minute),
        to: TimeOfDay(hour: to.hour, minute: to.minute),
        room: maps[i]['room'],
        priority: maps[i]['priority'],
        reminderTime: maps[i]['reminderTime'],
      );
    }).where((Meeting meeting) {
      return dateWithOutTime.compareTo(meeting.date) == 0;
    }).toList()
      ..sort((a, b) => b.priority.compareTo(a.priority));
    return list;
  }

  Future<void> updateMeeting(Meeting meeting) async {
    final db = await DBConfiguration.openDB();
    await db.update(
      'Meeting',
      meeting.toMap(),
      where: "id = ?",
      whereArgs: [meeting.id],
    );
  }

  Future<void> deleteMeeting(Meeting meeting) async {
    final db = await DBConfiguration.openDB();
    await db.delete(
      'Meeting',
      where: "id = ?",
      whereArgs: [meeting.id],
    );
  }
}
