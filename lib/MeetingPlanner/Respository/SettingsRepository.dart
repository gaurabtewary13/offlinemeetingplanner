import 'package:flutter/material.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Model/Settings.dart';
import 'package:offlinemeetingplanner/Misc/DBConfiguration.dart';
import 'package:sqflite/sqflite.dart';

class SettingsRepository {
  Future<void> insertSettings(Settings settings) async {
    final Database db = await DBConfiguration.openDB();
    await db.insert(
      'Settings',
      settings.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<Settings> fetchSettings() async {
    final Database db = await DBConfiguration.openDB();
    final List<Map<String, dynamic>> maps = await db.query(
      'Settings',
    );
    List list = List.generate(maps.length, (i) {
      List startSplit = maps[i]['officeStartTime'].split('-');
      List endSplit = maps[i]['officeEndTime'].split('-');

      return Settings(
        id: maps[i]['id'],
        officeStartTime:
            TimeOfDay(hour: int.parse(startSplit.first), minute: int.parse(startSplit.last)),
        officeEndTime: TimeOfDay(hour: int.parse(endSplit.first), minute: int.parse(endSplit.last)),
        room1Color: maps[i]['room1Color'],
        room2Color: maps[i]['room2Color'],
        room3Color: maps[i]['room3Color'],
        room4Color: maps[i]['room4Color'],
        isReminderOn: maps[i]['isReminderOn'] == 1,
      );
    });
    return list.first;
  }

  Future<void> updateSettings(Settings settings) async {
    final db = await DBConfiguration.openDB();
    await db.update(
      'Settings',
      settings.toMap(),
      where: "id = ?",
      whereArgs: [settings.id],
    );
  }
}
