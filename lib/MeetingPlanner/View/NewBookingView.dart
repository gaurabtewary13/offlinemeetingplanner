import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Bloc/NewBookingBloc.dart';
import 'package:offlinemeetingplanner/Misc/Constants.dart';
import 'package:offlinemeetingplanner/Misc/LocalNotificationHandler.dart';

class NewBookingView extends StatefulWidget {
  @override
  _NewBookingViewState createState() => _NewBookingViewState();
}

class _NewBookingViewState extends State<NewBookingView> {
  TextEditingController _titleController;
  TextEditingController _descController;
  TextEditingController _dateController;
  TextEditingController _startTimeController;
  TextEditingController _endTimeController;
  int _selectedRoom;
  int _selectedPriority;
  int _selectedReminderDuration;
  FocusNode _titleFocusNode;
  FocusNode _descFocusNode;

  @override
  void initState() {
    super.initState();
    context.read<NewBookingBloc>().add(
          FetchSettings(),
        );
    _titleFocusNode = FocusNode();
    _descFocusNode = FocusNode();
    _titleFocusNode.addListener(() {
      if (!_titleFocusNode.hasFocus) {
        context.read<NewBookingBloc>().add(
              UpdateTitle(title: _titleController.text),
            );
      }
    });

    _descFocusNode.addListener(() {
      if (!_descFocusNode.hasFocus) {
        context.read<NewBookingBloc>().add(
              UpdateDescription(description: _descController.text),
            );
      }
    });

    _titleController = TextEditingController();
    _descController = TextEditingController();
    _dateController = TextEditingController(
      text: DateFormat(meetingPlannerDateFormat).format(
        DateTime.now(),
      ),
    );
    _startTimeController = TextEditingController(
      text: DateFormat(meetingPlannerTimeFormat).format(
        DateTime.now(),
      ),
    );
    _endTimeController = TextEditingController(
      text: DateFormat(meetingPlannerTimeFormat).format(
        DateTime.now().add(
          Duration(minutes: 30),
        ),
      ),
    );
    _selectedRoom = _selectedPriority = _selectedReminderDuration = 0;
  }

  void _showAlert(
    BuildContext context,
    String content,
    VoidCallback action,
  ) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: Text(content),
        actions: [
          FlatButton(
            child: Text(okayText),
            onPressed: action,
          ),
        ],
      ),
    );
  }

  bool _isWithinOfficeHours(
    TimeOfDay time,
    TimeOfDay officeStartTime,
    TimeOfDay officeEndTime,
  ) {
    bool flag = ((time.hour > officeStartTime.hour ||
            (time.hour == officeStartTime.hour &&
                time.minute >= officeStartTime.minute)) &&
        (time.hour < officeEndTime.hour ||
            (time.hour == officeEndTime.hour &&
                time.minute <= officeEndTime.minute)));
    return flag;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewBookingBloc, NewBookingState>(
      buildWhen: (prev, cur) {
        if (cur.isSaved) {
          if (cur.settings.isReminderOn) {
            int reminderMin = (cur.meeting.reminderTime + 1) * 15;
            DateTime date = DateTime(
              cur.meeting.date.year,
              cur.meeting.date.month,
              cur.meeting.date.day,
              cur.meeting.from.hour,
              cur.meeting.from.minute,
            );
            Duration diff = date.difference(
              DateTime.now(),
            );
            Duration newDuration;
            String msgString;
            if (diff.inMinutes > reminderMin) {
              newDuration = diff - Duration(minutes: reminderMin);
              msgString = 'Meeting will start in $reminderMin minutes';
            } else {
              newDuration = Duration(seconds: 2);
              msgString = 'Meeting will start in ${diff.inMinutes} minutes';
            }

            LoginNotificationHandler.zonedScheduleNotification(
              cur.meeting.title,
              msgString,
              newDuration,
            );
          }

          _showAlert(
            context,
            meetingSavedMesage,
            () {
              Navigator.of(context).pop();
              Navigator.of(context).pop();
            },
          );
          return false;
        }
        return true;
      },
      builder: (BuildContext context, NewBookingState state) {
        _dateController.text = DateFormat(meetingPlannerDateFormat).format(
          state.meeting?.date ?? DateTime.now(),
        );
        _startTimeController.text = state.meeting?.from == null
            ? DateFormat(meetingPlannerTimeFormat).format(
                DateTime.now(),
              )
            : state.meeting.from.format(context);
        _endTimeController.text = state.meeting?.to == null
            ? DateFormat(meetingPlannerTimeFormat).format(
                DateTime.now(),
              )
            : state.meeting.to.format(context);
        _selectedRoom = state.meeting?.room ?? 0;
        _selectedPriority = state.meeting?.priority ?? 0;
        _selectedReminderDuration = state.meeting?.reminderTime ?? 0;

        return Scaffold(
          appBar: AppBar(
            title: Text(newBookingViewTitle),
            actions: [
              IconButton(
                icon: Icon(
                  Icons.save,
                ),
                onPressed: () {
                  FocusScope.of(context).unfocus();
                  if (state.meeting == null ||
                      (state.meeting.title?.isEmpty ?? true)) {
                    _showAlert(
                      context,
                      requiredFieldsNotEntered,
                      () {
                        Navigator.of(context).pop();
                      },
                    );
                  } else {
                    context.read<NewBookingBloc>().add(
                          SaveMeeting(meeting: state.meeting),
                        );
                  }
                },
              )
            ],
          ),
          body: SingleChildScrollView(
            child: Form(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextFormField(
                      controller: _titleController,
                      focusNode: _titleFocusNode,
                      style: TextStyle(fontSize: 13),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      decoration: InputDecoration(
                        hintText: 'Title',
                        labelText: 'Title *',
                        hintStyle: TextStyle(fontSize: 11),
                        labelStyle: TextStyle(fontSize: 11, color: Colors.grey),
                      ),
                      validator: (val) {
                        return val.length == 0 ? 'Title cant be empty' : null;
                      },
                      onEditingComplete: () {
                        FocusScope.of(context).unfocus();
                        context.read<NewBookingBloc>().add(
                              UpdateTitle(title: _titleController.text),
                            );
                      },
                    ),
                    TextFormField(
                      controller: _descController,
                      focusNode: _descFocusNode,
                      style: TextStyle(fontSize: 13),
                      decoration: InputDecoration(
                        hintText: 'Description',
                        labelText: 'Description',
                        hintStyle: TextStyle(fontSize: 11),
                        labelStyle: TextStyle(fontSize: 11, color: Colors.grey),
                      ),
                      onEditingComplete: () {
                        FocusScope.of(context).unfocus();
                        context.read<NewBookingBloc>().add(
                              UpdateDescription(
                                  description: _descController.text),
                            );
                      },
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          width: (MediaQuery.of(context).size.width - 64) / 3,
                          child: TextFormField(
                            controller: _dateController,
                            style: TextStyle(fontSize: 13),
                            decoration: InputDecoration(
                              hintText: 'Start Date',
                              labelText: 'Start Date *',
                              hintStyle: TextStyle(fontSize: 11),
                              labelStyle:
                                  TextStyle(fontSize: 11, color: Colors.grey),
                            ),
                            onTap: () async {
                              FocusScope.of(context).requestFocus(FocusNode());
                              DateTime date = await showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime.now(),
                                lastDate: DateTime(2100),
                              );
                              context.read<NewBookingBloc>().add(
                                    UpdateStartDate(date: date),
                                  );
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 16),
                          child: SizedBox(
                            width: (MediaQuery.of(context).size.width - 64) / 3,
                            child: TextFormField(
                              controller: _startTimeController,
                              style: TextStyle(fontSize: 13),
                              decoration: InputDecoration(
                                hintText: 'Start Time',
                                labelText: 'Start Time *',
                                hintStyle: TextStyle(fontSize: 11),
                                labelStyle:
                                    TextStyle(fontSize: 11, color: Colors.grey),
                              ),
                              onTap: () async {
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                                TimeOfDay time = await showTimePicker(
                                  context: context,
                                  initialTime: TimeOfDay.fromDateTime(
                                    DateTime.now(),
                                  ),
                                );
                                if (_isWithinOfficeHours(
                                    time,
                                    state.settings.officeStartTime,
                                    state.settings.officeEndTime)) {
                                  context.read<NewBookingBloc>().add(
                                        UpdateStartTime(time: time),
                                      );
                                } else {
                                  _showAlert(context,
                                      'Meeting time cant be outside office hour',
                                      () {
                                    Navigator.of(context).pop();
                                  });
                                }
                              },
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 16),
                          child: SizedBox(
                            width: (MediaQuery.of(context).size.width - 64) / 3,
                            child: TextFormField(
                              controller: _endTimeController,
                              style: TextStyle(fontSize: 13),
                              decoration: InputDecoration(
                                hintText: 'End Time',
                                labelText: 'End Time *',
                                hintStyle: TextStyle(fontSize: 11),
                                labelStyle:
                                    TextStyle(fontSize: 11, color: Colors.grey),
                              ),
                              onTap: () async {
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                                TimeOfDay time = await showTimePicker(
                                  context: context,
                                  initialTime: state.meeting?.from ??
                                      TimeOfDay.fromDateTime(
                                        DateTime.now().add(
                                          Duration(minutes: 30),
                                        ),
                                      ),
                                );
                                if (_isWithinOfficeHours(
                                    time,
                                    state.settings.officeStartTime,
                                    state.settings.officeEndTime)) {
                                  if (time.hour > state.meeting.from.hour ||
                                      (time.hour == state.meeting.from.hour &&
                                          time.minute >=
                                              state.meeting.from.minute)) {
                                    context.read<NewBookingBloc>().add(
                                          UpdateEndTime(time: time),
                                        );
                                  } else {
                                    _showAlert(context,
                                        'End time cant be before start time',
                                        () {
                                      Navigator.of(context).pop();
                                    });
                                  }
                                } else {
                                  _showAlert(context,
                                      'Meeting time cant be outside office hour',
                                      () {
                                    Navigator.of(context).pop();
                                  });
                                }
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 16),
                      child: Text(
                        'Select Room *',
                        style: TextStyle(fontSize: 11, color: Colors.grey),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 8),
                      child: SizedBox(
                        width: double.infinity,
                        child: CupertinoSegmentedControl(
                          groupValue: _selectedRoom,
                          padding: EdgeInsets.zero,
                          onValueChanged: (value) {
                            context.read<NewBookingBloc>().add(
                                  UpdateRoom(room: value),
                                );
                          },
                          children: const <int, Widget>{
                            0: Text('Room 1'),
                            1: Text('Room 2'),
                            2: Text('Room 3'),
                            3: Text('Room 4'),
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 16),
                      child: Text(
                        'Select Priority',
                        style: TextStyle(fontSize: 11, color: Colors.grey),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 8),
                      child: SizedBox(
                        width: double.infinity,
                        child: CupertinoSegmentedControl(
                          groupValue: _selectedPriority,
                          padding: EdgeInsets.zero,
                          onValueChanged: (value) {
                            context.read<NewBookingBloc>().add(
                                  UpdatePriority(priority: value),
                                );
                          },
                          children: const <int, Widget>{
                            0: Text('Low'),
                            1: Text('Medium'),
                            2: Text('High'),
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 16),
                      child: Text(
                        'Select Reminder',
                        style: TextStyle(fontSize: 11, color: Colors.grey),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 8),
                      child: SizedBox(
                        width: double.infinity,
                        child: CupertinoSegmentedControl(
                          groupValue: _selectedReminderDuration,
                          padding: EdgeInsets.zero,
                          onValueChanged: (value) {
                            context.read<NewBookingBloc>().add(
                                  UpdateReminderTime(reminderTime: value),
                                );
                          },
                          children: const <int, Widget>{
                            0: Text('15 Mins'),
                            1: Text('30 Mins'),
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
