import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Bloc/SettingsBloc.dart';
import 'package:offlinemeetingplanner/Misc/Utils.dart';

class SettingsView extends StatefulWidget {
  @override
  _SettingsViewState createState() => _SettingsViewState();
}

class _SettingsViewState extends State<SettingsView> {
  TextEditingController _startTimeController = TextEditingController();
  TextEditingController _endTimeController = TextEditingController();

  TextEditingController _meetingRoom1ColorController =
      TextEditingController(text: '112233');
  TextEditingController _meetingRoom2ColorController =
      TextEditingController(text: '223344');
  TextEditingController _meetingRoom3ColorController =
      TextEditingController(text: '334455');
  TextEditingController _meetingRoom4ColorController =
      TextEditingController(text: '445566');
  bool _isReminderOn = true;

  @override
  void initState() {
    super.initState();
    context.read<SettingsBloc>().add(
          FetchSettings(),
        );
  }

  Widget _roomColorWidget(
    String title,
    TextEditingController controller,
    Function(String) onSubmit,
  ) {
    return Row(
      children: [
        Expanded(
          child: TextFormField(
            controller: controller,
            style: TextStyle(fontSize: 13),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            decoration: InputDecoration(
              hintText: 'RRGGBB, E.g. - aabbcc',
              labelText: title,
              hintStyle: TextStyle(fontSize: 11),
              labelStyle: TextStyle(fontSize: 11, color: Colors.grey),
            ),
            validator: (val) {
              return val.length == 0 ? 'Title cant be empty' : null;
            },
            onFieldSubmitted: onSubmit,
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 30),
          width: 30,
          height: 30,
          color: HexColor.fromHex('#${controller.text}'),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsBloc, SettingsState>(
      builder: (BuildContext context, state) {
        if (state.settings != null) {
          _startTimeController.text =
              state.settings.officeStartTime.format(context);
          _endTimeController.text =
              state.settings.officeEndTime.format(context);
          _meetingRoom1ColorController.text = state.settings.room1Color;
          _meetingRoom2ColorController.text = state.settings.room2Color;
          _meetingRoom3ColorController.text = state.settings.room3Color;
          _meetingRoom4ColorController.text = state.settings.room4Color;
          _isReminderOn = state.settings.isReminderOn;
        }

        return Scaffold(
          appBar: AppBar(
            title: Text('Settings'),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 0),
                        child: SizedBox(
                          width: (MediaQuery.of(context).size.width - 64) / 3,
                          child: TextFormField(
                            controller: _startTimeController,
                            style: TextStyle(fontSize: 13),
                            decoration: InputDecoration(
                              hintText: 'Start Time',
                              labelText: 'Start Time *',
                              hintStyle: TextStyle(fontSize: 11),
                              labelStyle:
                                  TextStyle(fontSize: 11, color: Colors.grey),
                            ),
                            onTap: () async {
                              FocusScope.of(context).requestFocus(FocusNode());
                              TimeOfDay time = await showTimePicker(
                                context: context,
                                initialTime: TimeOfDay(hour: 9, minute: 0),
                              );
                              if (time != null) {
                                context.read<SettingsBloc>().add(
                                      UpdateOfficeStartTime(time: time),
                                    );
                              }
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: SizedBox(
                          width: (MediaQuery.of(context).size.width - 64) / 3,
                          child: TextFormField(
                            controller: _endTimeController,
                            style: TextStyle(fontSize: 13),
                            decoration: InputDecoration(
                              hintText: 'End Time',
                              labelText: 'End Time *',
                              hintStyle: TextStyle(fontSize: 11),
                              labelStyle:
                                  TextStyle(fontSize: 11, color: Colors.grey),
                            ),
                            onTap: () async {
                              FocusScope.of(context).requestFocus(FocusNode());
                              TimeOfDay time = await showTimePicker(
                                context: context,
                                initialTime: state.settings?.officeStartTime ??
                                    TimeOfDay.fromDateTime(
                                      DateTime.now().add(
                                        Duration(minutes: 30),
                                      ),
                                    ),
                              );
                              if (time != null) {
                                context.read<SettingsBloc>().add(
                                      UpdateOfficeEndTime(time: time),
                                    );
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 16.0),
                    child: Text(
                      'Meeting Room Color Code',
                      style: TextStyle(fontSize: 11, color: Colors.grey),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: _roomColorWidget(
                        'Meeting Room 1', _meetingRoom1ColorController,
                        (colorCode) {
                      FocusScope.of(context).unfocus();
                      context.read<SettingsBloc>().add(
                            UpdateRoom1Color(colorCode: colorCode),
                          );
                    }),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: _roomColorWidget(
                        'Meeting Room 2', _meetingRoom2ColorController,
                        (colorCode) {
                      FocusScope.of(context).unfocus();
                      context.read<SettingsBloc>().add(
                            UpdateRoom2Color(colorCode: colorCode),
                          );
                    }),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: _roomColorWidget(
                        'Meeting Room 3', _meetingRoom3ColorController,
                        (colorCode) {
                      FocusScope.of(context).unfocus();
                      context.read<SettingsBloc>().add(
                            UpdateRoom3Color(colorCode: colorCode),
                          );
                    }),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: _roomColorWidget(
                        'Meeting Room 4', _meetingRoom4ColorController,
                        (colorCode) {
                      FocusScope.of(context).unfocus();
                      context.read<SettingsBloc>().add(
                            UpdateRoom4Color(colorCode: colorCode),
                          );
                    }),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 16.0),
                    child: Text(
                      'Reminder on/off',
                      style: TextStyle(fontSize: 11, color: Colors.grey),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: CupertinoSwitch(
                      activeColor: Colors.purple,
                      value: _isReminderOn,
                      onChanged: (bool value) {
                        _isReminderOn = !_isReminderOn;
                        context.read<SettingsBloc>().add(
                              UpdateReminder(flag: _isReminderOn),
                            );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
