import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Bloc/MeetingDetailsBloc.dart';

import '../../Misc/Utils.dart';
import '../Model/Meeting.dart';
import '../Model/Settings.dart';

class MeetingDetailView extends StatefulWidget {
  MeetingDetailView({
    @required this.meeting,
    @required this.settings,
  });

  final Meeting meeting;
  final Settings settings;
  @override
  _MeetingDetailViewState createState() => _MeetingDetailViewState();
}

class _MeetingDetailViewState extends State<MeetingDetailView> {
  Color _roomColor() {
    String color;
    switch (widget.meeting.room) {
      case 0:
        color = widget.settings?.room1Color ?? 'ffffff';
        break;
      case 1:
        color = widget.settings?.room2Color ?? 'ffffff';
        break;
      case 2:
        color = widget.settings?.room3Color ?? 'ffffff';
        break;
      default:
        color = widget.settings?.room4Color ?? 'ffffff';
        break;
        break;
    }
    return HexColor.fromHex(color);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<MeetingDetailsBloc, MeetingDetailsState>(
      listener: (BuildContext context, MeetingDetailsState state) {
        if (state.isDeleted) {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              content: Text('Meeting is successfully deleted.'),
              actions: [
                FlatButton(
                  child: Text('Okay'),
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        }
      },
      child: Scaffold(
        backgroundColor: _roomColor(),
        appBar: AppBar(
          title: Text('Meeting Details'),
          actions: [
            IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (alertContext) => AlertDialog(
                    // title: Text(title),
                    content: Text('Do you want to delete this meeting?'),
                    actions: [
                      FlatButton(
                        child: Text('No'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      FlatButton(
                        child: Text('Yes'),
                        onPressed: () {
                          Navigator.of(context).pop();
                          context.read<MeetingDetailsBloc>().add(
                                DeleteMeeting(meeting: widget.meeting),
                              );
                        },
                      ),
                    ],
                  ),
                );
              },
            )
          ],
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Text(
                  widget.meeting.title,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                widget.meeting.desc != null && widget.meeting.desc.length > 0
                    ? Text(
                        widget.meeting.desc,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                        ),
                      )
                    : Container(),
                Text(
                  'Date - ${DateFormat('dd MMM yyyy').format(widget.meeting.date)}',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                Text(
                  'From - ${widget.meeting.from.format(context)}',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                Text(
                  'To - ${widget.meeting.to.format(context)}',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                Text(
                  'From - Meeting Room ${widget.meeting.room + 1}',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                Text(
                  'Priority - ${widget.meeting.priority == 0 ? 'Low' : widget.meeting.priority == 1 ? 'Medium' : 'High'}',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                Text(
                  'Reminder - ${(widget.meeting.reminderTime + 1) * 15} minutes',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
