import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Bloc/HomeBloc.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Model/Meeting.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Model/Settings.dart';
import 'package:offlinemeetingplanner/Misc/Constants.dart';
import 'package:intl/intl.dart';
import 'package:offlinemeetingplanner/Misc/Utils.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  DateTime _selectedDate = DateTime.now();
  @override
  void initState() {
    super.initState();
    context.read<HomeBloc>().add(
          FetchData(
              date: DateTime(
            DateTime.now().year,
            DateTime.now().month,
            DateTime.now().day,
          )),
        );
  }

  Widget _dateSelectionWidget(DateTime selectedDate) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 16),
          child: Text(chooseDateTitle),
        ),
        InkWell(
          splashColor: Colors.transparent,
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 8),
            width: double.infinity,
            height: 50,
            decoration: BoxDecoration(
              color: Colors.purple,
              borderRadius: BorderRadius.circular(4),
            ),
            child: Center(
              child: Text(
                DateFormat(meetingPlannerDateFormat).format(selectedDate),
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.w800,
                    color: Colors.white),
              ),
            ),
          ),
          onTap: () async {
            DateTime date = await showDatePicker(
              context: context,
              initialDate: _selectedDate ?? DateTime.now(),
              firstDate: DateTime.now(),
              lastDate: DateTime(2100),
            );
            _selectedDate = date;
            context.read<HomeBloc>().add(
                  FetchMeetings(date: date),
                );
          },
        )
      ],
    );
  }

  Widget _meeingRow(
    Meeting meeting,
    Settings settings,
  ) {
    String color;
    switch (meeting.room) {
      case 0:
        color = settings?.room1Color ?? 'ffffff';
        break;
      case 1:
        color = settings?.room2Color ?? 'ffffff';
        break;
      case 2:
        color = settings?.room3Color ?? 'ffffff';
        break;
      default:
        color = settings?.room4Color ?? 'ffffff';
        break;
        break;
    }
    return InkWell(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 4),
        padding: EdgeInsets.symmetric(vertical: 8),
        width: double.infinity,
        height: 60,
        decoration: BoxDecoration(
          color: HexColor.fromHex(color),
          borderRadius: BorderRadius.circular(24),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 8.0,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 16),
                      child: Text(
                        meeting.title ?? '',
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  meeting.from == null
                      ? ''
                      : 'Start - ${meeting.from.format(context)}',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4),
                  child: Text(
                    meeting.to == null
                        ? ''
                        : 'End - ${meeting.to.format(context)}',
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.battery_alert_rounded,
                    color: _priorityColor(meeting.priority),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      onTap: () async {
        await Navigator.of(context).pushNamed(detailViewRoute, arguments: [
          meeting,
          settings,
        ]);
        DateTime date = _selectedDate ?? DateTime.now();
        context.read<HomeBloc>().add(
              FetchMeetings(date: date),
            );
      },
    );
  }

  Color _priorityColor(int priority) {
    switch (priority) {
      case 1:
        return Colors.orange;
        break;
      case 2:
        return Colors.red;
        break;
      default:
        return Colors.green;
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeViewState>(
      builder: (BuildContext context, HomeViewState state) {
        return Scaffold(
          appBar: AppBar(
            title: Text(homeViewTitle),
            actions: [
              IconButton(
                icon: Icon(
                  Icons.settings,
                ),
                onPressed: () async {
                  await Navigator.of(context).pushNamed(settingsViewRoute);
                  context.read<HomeBloc>().add(
                        FetchData(
                          date: DateTime(
                            DateTime.now().year,
                            DateTime.now().month,
                            DateTime.now().day,
                          ),
                        ),
                      );
                },
              )
            ],
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(
              Icons.add,
            ),
            onPressed: () async {
              await Navigator.of(context).pushNamed(
                addMeetingViewRoute,
              );
              context.read<HomeBloc>().add(
                    FetchData(
                      date: DateTime(
                        DateTime.now().year,
                        DateTime.now().month,
                        DateTime.now().day,
                      ),
                    ),
                  );
            },
          ),
          body: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _dateSelectionWidget(
                  state.selectedDate,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(upcomingMeetingsTitle),
                ),
                (state.meetings == null || state.meetings.length == 0)
                    ? Container(
                        width: double.infinity,
                        height: 240,
                        decoration: BoxDecoration(
                          color: Colors.purple.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(24),
                        ),
                        child: Center(
                          child: Text(
                            noMeetingHint,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: Colors.purple,
                            ),
                          ),
                        ),
                      )
                    : Expanded(
                        child: ListView.builder(
                          itemCount: state.meetings?.length ?? 0,
                          itemBuilder: (BuildContext context, int index) {
                            return _meeingRow(
                              state.meetings[index],
                              state.settings,
                            );
                          },
                        ),
                      ),
              ],
            ),
          ),
        );
      },
    );
  }
}
