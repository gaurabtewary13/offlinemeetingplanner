import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Settings {
  Settings({
    @required this.id,
    this.officeStartTime,
    this.officeEndTime,
    this.room1Color,
    this.room2Color,
    this.room3Color,
    this.room4Color,
    this.isReminderOn,
  });

  int id;
  TimeOfDay officeStartTime;
  TimeOfDay officeEndTime;
  String room1Color;
  String room2Color;
  String room3Color;
  String room4Color;
  bool isReminderOn;

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'officeStartTime': '${officeStartTime.hour}-${officeStartTime.minute}',
      'officeEndTime': '${officeEndTime.hour}-${officeEndTime.minute}',
      'room1Color': room1Color,
      'room2Color': room2Color,
      'room3Color': room3Color,
      'room4Color': room4Color,
      'isReminderOn': isReminderOn ? 1 : 0 ,
    };
  }
}
