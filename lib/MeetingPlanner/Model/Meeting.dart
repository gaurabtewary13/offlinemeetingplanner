import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Meeting {
  Meeting({
    @required this.id,
    this.title,
    this.desc,
    this.date,
    this.from,
    this.to,
    this.room,
    this.priority,
    this.reminderTime,
  });

  int id;
  String title;
  String desc;
  DateTime date;
  TimeOfDay from;
  TimeOfDay to;
  int room;
  int priority;
  int reminderTime;

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'desc': desc,
      'fromTime': DateTime(date.year, date.month, date.day, from.hour, from.minute)
          .millisecondsSinceEpoch,
      'toTime':
          DateTime(date.year, date.month, date.day, to.hour, to.minute)
              .millisecondsSinceEpoch,
      'room': room,
      'priority': priority,
      'reminderTime': reminderTime,
    };
  }
}
