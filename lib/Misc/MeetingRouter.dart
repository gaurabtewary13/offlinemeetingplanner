import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Bloc/HomeBloc.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Bloc/MeetingDetailsBloc.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Bloc/NewBookingBloc.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Bloc/SettingsBloc.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Respository/MeetingRepository.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/Respository/SettingsRepository.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/View/HomeView.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/View/MeetingDetailView.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/View/NewBookingView.dart';
import 'package:offlinemeetingplanner/MeetingPlanner/View/SettingsView.dart';
import '../MeetingPlanner/Respository/MeetingRepository.dart';
import 'Constants.dart';

class MeetingRouter extends MaterialPageRoute {
  MeetingRouter(
      {WidgetBuilder builder,
      RouteSettings settings,
      bool fullscreenDialog = false})
      : super(
          builder: builder,
          settings: settings,
          fullscreenDialog: fullscreenDialog,
        );

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (fullscreenDialog) {
      return SlideTransition(
        position: Tween<Offset>(
          begin: const Offset(0, 1),
          end: Offset.zero,
        ).animate(animation),
        child: child,
      );
    } else {
      return SlideTransition(
        position: Tween<Offset>(
          begin: const Offset(1, 0),
          end: Offset.zero,
        ).animate(animation),
        child: child,
      );
    }
  }

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case homeViewRoute:
        return MeetingRouter(
          builder: (_) => BlocProvider(
            create: (BuildContext context) {
              return HomeBloc(
                repository: MeetingRepository(),
                settingsRepository: SettingsRepository(),
              );
            },
            child: HomeView(),
          ),
        );
        break;
      case addMeetingViewRoute:
        return MeetingRouter(
          fullscreenDialog: true,
          builder: (_) => BlocProvider(
            create: (BuildContext context) {
              return NewBookingBloc(
                repository: MeetingRepository(),
                settingsRepository: SettingsRepository(),
              );
            },
            child: NewBookingView(),
          ),
        );
        break;
      case settingsViewRoute:
        return MeetingRouter(
          fullscreenDialog: true,
          builder: (_) => BlocProvider(
            create: (BuildContext context) {
              return SettingsBloc(
                repository: SettingsRepository(),
              );
            },
            child: SettingsView(),
          ),
        );
        break;
      case detailViewRoute:
        List args = settings.arguments;
        return MeetingRouter(
          builder: (_) => BlocProvider(
            create: (BuildContext context) {
              return MeetingDetailsBloc(
                repository: MeetingRepository(),
              );
            },
            child: MeetingDetailView(
              meeting: args[0],
              settings: args[1],
            ),
          ),
        );
        break;
      default:
        return null;
    }
  }
}
