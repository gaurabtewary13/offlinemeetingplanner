import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DBConfiguration {
  static Future<Database> openDB() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'demo.db');
    Database database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute(
        'CREATE TABLE Meeting (id INTEGER PRIMARY KEY, title TEXT, desc TEXT, fromTime INTEGER, toTime INTEGER, room INTEGER, priority INTEGER, reminderTime INTEGER)',
      );
      await db.execute(
        'CREATE TABLE Settings (id INTEGER PRIMARY KEY, officeStartTime TEXT, officeEndTime TEXT, room1Color TEXT, room2Color TEXT, room3Color TEXT, room4Color TEXT, isReminderOn INTEGER)',
      );

      await db.transaction((txn) async {
        await txn.rawInsert(
            'INSERT INTO Settings(id, officeStartTime, officeEndTime, room1Color, room2Color, room3Color, room4Color, isReminderOn) VALUES(0000, "09-00", "17-00", "79d49f", "5c718a", "b54881", "a8a832", 1)');
      });
    });

    return database;
  }
}
