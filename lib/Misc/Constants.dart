// Routes
const String homeViewRoute = 'homeViewRoute';
const String addMeetingViewRoute = 'addMeetingViewRoute';
const String settingsViewRoute = 'settingsViewRoute';
const String detailViewRoute = 'detailViewRoute';

// Common Strings
const String okayText = 'Okay';

// HomeView strings
const String homeViewTitle = 'Home';
const String newBookingViewTitle = 'Add New Booking';
const String chooseDateTitle = 'Choose a date';
const String upcomingMeetingsTitle = 'Upcoming Meetings';
const String noMeetingHint = 'No meeting to display for the selected day.\n\nTap on the + icon to add a meeting \n\nor\n\n Choose another date.';

// New Meeting Strings
const String meetingSavedMesage = 'Meeting saved successfully';
const String requiredFieldsNotEntered = 'Please enter all the required fields';

const String meetingPlannerDateFormat = 'dd MMM yyyy';
const String meetingPlannerTimeFormat = 'hh:mm aa';