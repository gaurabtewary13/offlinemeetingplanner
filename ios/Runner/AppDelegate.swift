import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GeneratedPluginRegistrant.register(with: self)
    
    if UserDefaults.standard.bool(forKey: "Notification") {
        UIApplication.shared.cancelAllLocalNotifications()
        UserDefaults.standard.set(true, forKey: "Notification")
    }
    
    
    if #available(iOS 10.0, *) {
        UNUserNotificationCenter.current().delegate = self
    } else {
        // Fallback on earlier versions
    }
    
    let controller = self.window.rootViewController as! FlutterViewController
    let channel = FlutterMethodChannel.init(name: "dexterx.dev/flutter_local_notifications_example", binaryMessenger: controller.binaryMessenger)
    channel.setMethodCallHandler { (call, result) in
        if call.method == "getTimeZoneName" {
            result(TimeZone.current.identifier)
        }
    }
    
    /*
     
     [GeneratedPluginRegistrant registerWithRegistry:self];
        // cancel old notifications that were scheduled to be periodically shown upon a reinstallation of the app
        if(![[NSUserDefaults standardUserDefaults]objectForKey:@"Notification"]){
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"Notification"];
        }
        if(@available(iOS 10.0, *)) {
            [UNUserNotificationCenter currentNotificationCenter].delegate = (id<UNUserNotificationCenterDelegate>) self;
        }
        
        FlutterViewController* controller = (FlutterViewController*)self.window.rootViewController;

        FlutterMethodChannel* channel = [FlutterMethodChannel
                                                methodChannelWithName:@"dexterx.dev/flutter_local_notifications_example"
                                                binaryMessenger:controller.binaryMessenger];

        [channel setMethodCallHandler:^(FlutterMethodCall* call, FlutterResult result) {
            if([@"getTimeZoneName" isEqualToString:call.method]) {
                result([[NSTimeZone localTimeZone] name]);
            }
        }];
     
     */
    
    
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
